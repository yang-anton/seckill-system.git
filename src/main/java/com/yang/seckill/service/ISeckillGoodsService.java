package com.yang.seckill.service;

import com.yang.seckill.pojo.SeckillGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
