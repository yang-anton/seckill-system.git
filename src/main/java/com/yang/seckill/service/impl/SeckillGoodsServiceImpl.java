package com.yang.seckill.service.impl;

import com.yang.seckill.pojo.SeckillGoods;
import com.yang.seckill.mapper.SeckillGoodsMapper;
import com.yang.seckill.service.ISeckillGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
@Service
public class SeckillGoodsServiceImpl extends ServiceImpl<SeckillGoodsMapper, SeckillGoods> implements ISeckillGoodsService {

}
