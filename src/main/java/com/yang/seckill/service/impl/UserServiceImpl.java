package com.yang.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.seckill.exception.GlobalException;
import com.yang.seckill.mapper.UserMapper;
import com.yang.seckill.pojo.User;
import com.yang.seckill.service.IUserService;
import com.yang.seckill.utils.CookieUtil;
import com.yang.seckill.utils.MD5Util;
import com.yang.seckill.utils.UUIDUtil;
import com.yang.seckill.utils.ValidatorUtil;
import com.yang.seckill.vo.LoginVo;
import com.yang.seckill.vo.RegisterVo;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yang
 * @since 2022-09-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 登录
     * @param loginVo 登录参数
     * @param request
     * @param response
     * @return
     */
    @Override
    public RespBean doLogin(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response) {

        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();


        //检测登录输入是否合格
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            //return RespBean.error(RespBeanEnum.LOGIN_ERROR);
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }

        if (!ValidatorUtil.isMobile(mobile)){
            //return RespBean.error(RespBeanEnum.MOBILE_ERROR);
            throw new GlobalException(RespBeanEnum.MOBILE_ERROR);
        }

        //根据手机号获取用户
        User user = userMapper.selectById(mobile);
        if (null == user){
            //return RespBean.error(RespBeanEnum.LOGIN_ERROR);
            throw new  GlobalException(RespBeanEnum.LOGIN_ERROR);
        }

        //判断密码是否正确
        if (!MD5Util.backendPassToDBPass(password,user.getSlat()).equals(user.getPassword())){
            //return RespBean.error(RespBeanEnum.LOGIN_ERROR);
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }

        //生成cookie
        String ticket = UUIDUtil.uuid();

        //将用户信息存放到session
        //request.getSession().setAttribute(ticket,user);

        //将用户信息存入redis中
        redisTemplate.opsForValue().set("user:" + ticket,user);

        CookieUtil.setCookie(request,response,"userTicket",ticket);
        return RespBean.success(ticket);
    }


    /**
     * 注册
     * @param registerVo 用户
     * @return
     */
    @Override
    public RespBean register(RegisterVo registerVo) {

        String id = registerVo.getId();
        String password = registerVo.getPassword();
        String rePassword = registerVo.getRePassword();

        if (StringUtils.isEmpty(id)||StringUtils.isEmpty(password)||StringUtils.isEmpty(rePassword)){
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }

        if (!ValidatorUtil.isMobile(id)){
            throw new GlobalException(RespBeanEnum.MOBILE_ERROR);
        }

        if (!password.equals(rePassword)){
            throw new GlobalException(RespBeanEnum.REGISTER_PASSWORD_ERROR);
        }

        User result = userMapper.selectById(id);
        if(result != null){
            throw new GlobalException(RespBeanEnum.REGISTER_USER_EXIT_ERROR);
        }

        User user = new User();

        String md5Password = MD5Util.inputPassToDBPass(password,"1a2b3c4d");
        Long uId = Long.valueOf(id);

        user.setId(uId);
        user.setPassword(md5Password);
        user.setNickname("user"+uId);
        user.setSlat("1a2b3c4d");
        user.setRegisterDate(new Date());
        user.setLoginCount(1);

        userMapper.insert(user);

        System.out.println("注册成功!");

        return RespBean.success();
    }


    /**
     * 根据cookie获取用户
     * @param userTicket
     * @param request
     * @param response
     * @return
     */
    @Override
    public User getUserByCookie(String userTicket,HttpServletRequest request, HttpServletResponse response) {

        if(StringUtils.isEmpty(userTicket)){
            return null;
        }

        User user = (User) redisTemplate.opsForValue().get("user:" + userTicket);
        if (user != null){
            CookieUtil.setCookie(request,response,"userTicket",userTicket);
        }
        return user;
    }

    /**
     * 更新用户密码
     * @param userTicket
     * @param password
     * @param request
     * @param response
     * @return
     */
    @Override
    public RespBean updatePassword(String userTicket, String password, HttpServletRequest request, HttpServletResponse response) {

        User user = getUserByCookie(userTicket, request, response);
        if (user == null){
            throw new GlobalException(RespBeanEnum.MOBILE_NOT_EXIT);
        }
        user.setPassword(MD5Util.inputPassToDBPass(password,user.getSlat()));
        int result = userMapper.updateById(user);
        if (1==result){
            //删除redis
            redisTemplate.delete("user:"+userTicket);
            return RespBean.success();
        }

        return RespBean.error(RespBeanEnum.PASSWORD_UPDATE_FAIL);
    }

}
