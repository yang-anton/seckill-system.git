package com.yang.seckill.service;

import com.yang.seckill.pojo.SeckillOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.seckill.pojo.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
public interface ISeckillOrderService extends IService<SeckillOrder> {

    /**
     * 获取秒杀结果
     * @param user
     * @param goodsId
     * @return orderId：成功；-1：秒杀失败；0：排队中
     */
    Long getResult(User user, Long goodsId);
}
