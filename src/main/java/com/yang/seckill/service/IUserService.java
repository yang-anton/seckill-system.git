package com.yang.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.seckill.pojo.User;
import com.yang.seckill.vo.LoginVo;
import com.yang.seckill.vo.RegisterVo;
import com.yang.seckill.vo.RespBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yang
 * @since 2022-09-05
 */
public interface IUserService extends IService<User> {

    /**
     * 登录
     * @param loginVo 登录参数
     * @param request
     * @param response
     * @return
     */
    RespBean doLogin(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 注册
     * @param user 用户
     * @return
     */
    RespBean register(RegisterVo user);


    /**
     * 根据cookie获取用户
     * @param userTicket
     * @return
     */
    User getUserByCookie(String userTicket,HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新密码
     * @return
     */
    RespBean updatePassword(String userTicket, String password, HttpServletRequest request, HttpServletResponse response);
}
