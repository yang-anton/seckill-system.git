package com.yang.seckill.config;


import com.yang.seckill.pojo.User;
import com.yang.seckill.service.IUserService;
import com.yang.seckill.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 自定义用户参数
@Component
public class UserArgumentResolver implements HandlerMethodArgumentResolver {

    @Autowired
    private IUserService userService;

    // 校验是否为User类，假如返回true，才会执行下面的resolveArgument方法
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        Class<?> clazz = parameter.getParameterType();
        return clazz == User.class;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

//        // 通过webRequest获取HttpServletRequest类的request
//        HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
//        // 通过webRequest获取HttpServletResponse类的response
//        HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
//
//        String ticket = CookieUtil.getCookieValue(request, "userTicket");
//
//        if (StringUtils.isEmpty(ticket)){
//            return null;
//        }
//
//        return userService.getUserByCookie(ticket,request,response);
        return UserContext.getUser();
    }
}

