package com.yang.seckill.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


// MVC配置类
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private UserArgumentResolver userArgumentResolver;
    @Autowired
    private AccessLimitInterceptor accessLimitInterceptor;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(userArgumentResolver);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(accessLimitInterceptor);
    }
}

/**按照目前的写法来说，登录完成之后，后续作的每一步操作都需要判断用户是否登录，每一个接口都要判断
 * ticket有没有，根据这个ticket去redis里面获取信息，再判断用户是否存在，如果这些步骤都没有问题，才会真正去执行该接口想要执行的方法。如果
 * 每个接口都要去写这些步骤，那么就会很麻烦，使得项目显得很臃肿、繁琐。解决方法：在接口方法那里传入一个User对象的参数，在对此User对象参数
 * 进行接收之前就对其进行了非空校验，若校验通过，则让其传入此User对象参数                                                               */