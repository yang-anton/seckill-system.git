package com.yang.seckill.utils;


import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

/**
 * MD5工具类
 */
@Component
public class MD5Util {

    public static String md5(String src){
        return DigestUtils.md5Hex(src);
    }

    public static  final String salt = "1a2b3c4d";

    /**
     * 第一次加密：前端输入的密码加密转为后端密码
     * @param inputPass 前端输入的密码
     * @return 第一次加密后的密码
     */
    public static String inputPassToBackendPass(String inputPass){
        String str = "" + salt.charAt(0) + salt.charAt(2) + inputPass + salt.charAt(5) + salt.charAt(4) ;
        return md5(str);
    }

    /**
     * 第二次加密：后端加密后的密码再次加密存入数据库
     * @param backendPass 后端加密后的密码
     * @param salt 存入数据库的盐值
     * @return 第二次加密后的密码
     */
    public static String backendPassToDBPass(String backendPass, String salt){
        String str = "" + salt.charAt(0) + salt.charAt(2) + backendPass + salt.charAt(5) + salt.charAt(4);
        return md5(str);
    }


    /**
     * 直接前端输入的密码经过2次加密后存入数据库的密码
     * @param inputPass 前端输入的密码
     * @param salt 存入数据库的盐值
     * @return 存入数据库的密码
     */
    public static String inputPassToDBPass(String inputPass, String salt){
        String backendPass = inputPassToBackendPass(inputPass);
        String dbPass = backendPassToDBPass(backendPass,salt);
        return dbPass;
    }


    public static void main(String[] args) {
        System.out.println("前端进行加密后的密码：" + inputPassToBackendPass("123456"));
        System.out.println("后端传入数据库进行加密后的密码：" + backendPassToDBPass(inputPassToBackendPass("123456"),"1a2b3c4d"));
        System.out.println("经过2次加密后的密码：" + inputPassToDBPass("654321","1a2b3c4d"));
    }

}
