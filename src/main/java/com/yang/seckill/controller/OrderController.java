package com.yang.seckill.controller;


import com.yang.seckill.pojo.User;
import com.yang.seckill.service.IOrderService;
import com.yang.seckill.vo.OrderDetailVo;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @RequestMapping("/detail")
    @ResponseBody
    public RespBean detail(User user,Long orderId){

        if (user == null){
            return RespBean.error(RespBeanEnum.SESSION_ERROR);
        }

        OrderDetailVo orderDetail =  orderService.detail(orderId);
        return RespBean.success(orderDetail);
    }

}
