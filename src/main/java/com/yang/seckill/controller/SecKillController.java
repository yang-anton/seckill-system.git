package com.yang.seckill.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wf.captcha.ArithmeticCaptcha;
import com.yang.seckill.config.AccessLimit;
import com.yang.seckill.exception.GlobalException;
import com.yang.seckill.pojo.Order;
import com.yang.seckill.pojo.SecKillMessage;
import com.yang.seckill.pojo.SeckillOrder;
import com.yang.seckill.pojo.User;
import com.yang.seckill.rabbitmq.MQSender;
import com.yang.seckill.service.IGoodsService;
import com.yang.seckill.service.IOrderService;
import com.yang.seckill.service.ISeckillOrderService;
import com.yang.seckill.utils.JsonUtil;
import com.yang.seckill.vo.GoodsVo;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Controller
@RequestMapping("/secKill")
public class SecKillController implements InitializingBean {

    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private ISeckillOrderService seckillOrderService;
    @Autowired
    private IOrderService orderService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MQSender mqSender;
    @Autowired
    private RedisScript<Long> script;

    private Map<Long,Boolean> EmptyStockMap = new HashMap<>();

    /**
     * 秒杀
     * @param model
     * @param user
     * @param goodsId
     * @return
     */
    @RequestMapping("/doSecKill2")
    public String doSecKill2(Model model, User user, Long goodsId){

        if (user == null){
            return "login";
        }

        model.addAttribute("user",user);
        GoodsVo goods = goodsService.findGoodsVoByGoodsId(goodsId);

        //判断库存
        if (goods.getStockCount() < 1){
            model.addAttribute("error", RespBeanEnum.EMPTY_STOCK.getMessage());
            System.out.println("error3");
            return "secKillFail";
        }

        //判断是否重复抢购
        SeckillOrder seckillOrder = seckillOrderService.getOne(new QueryWrapper<SeckillOrder>().eq("user_id", user.getId()).eq("goods_id", goodsId));
        if (seckillOrder != null){
            model.addAttribute("error",RespBeanEnum.REPEAT_ERROR.getMessage());
            return "secKillFail";
        }

        Order order = orderService.secKill(user,goods);
        model.addAttribute("order",order);
        model.addAttribute("goods",goods);

        return "orderDetail";
    }


    /**
     * 秒杀
     * @param user
     * @param goodsId
     * @return
     */
    @RequestMapping(value = "/{path}/doSecKill", method = RequestMethod.POST)
    @ResponseBody
    public RespBean doSecKill(@PathVariable String path, User user,Long goodsId){

        if (user == null){
            return RespBean.error(RespBeanEnum.SESSION_ERROR);
        }

        ValueOperations valueOperations = redisTemplate.opsForValue();
        boolean check = orderService.checkPath(user,goodsId,path);
        if (!check){
            return RespBean.error(RespBeanEnum.REQUEST_ILLEGAL);
        }

//        GoodsVo goods = goodsService.findGoodsVoByGoodsId(goodsId);
//        //判断库存
//        if (goods.getStockCount() < 1){
//            model.addAttribute("error", RespBeanEnum.EMPTY_STOCK.getMessage());
//            return RespBean.error(RespBeanEnum.EMPTY_STOCK);
//        }
//
//        //判断是否重复抢购
////        SeckillOrder seckillOrder = seckillOrderService.getOne(new QueryWrapper<SeckillOrder>().eq("user_id", user.getId()).eq("goods_id", goodsId));
//        SeckillOrder seckillOrder  =(SeckillOrder) redisTemplate.opsForValue().get("order:" + user.getId() + ":" + goods.getId());
//
//        if (seckillOrder != null){
//            model.addAttribute("error",RespBeanEnum.REPEAT_ERROR.getMessage());
//            return RespBean.error(RespBeanEnum.REPEAT_ERROR);
//        }
//
//        Order order = orderService.secKill(user,goods);
//        return RespBean.success(order);

        //判断是否重复抢购
        SeckillOrder seckillOrder  =
                (SeckillOrder) redisTemplate.opsForValue().get("order:" + user.getId() + ":" + goodsId);
        if (seckillOrder != null){
            return RespBean.error(RespBeanEnum.REPEAT_ERROR);
        }


        //内存标记，减少redis访问
        if (EmptyStockMap.get(goodsId)){
            return RespBean.error(RespBeanEnum.EMPTY_STOCK);
        }

        //预减库存
//        Long stock = valueOperations.decrement("secKillGoods:" + goodsId);
        Long stock = (Long) redisTemplate.execute(script, Collections.singletonList("secKillGoods:" + goodsId), Collections.EMPTY_LIST);

        if (stock < 0){
            EmptyStockMap.put(goodsId,true);
            //递加操作防止redis库存变为负数
            valueOperations.increment("secKillGoods:" + goodsId);
            return RespBean.error(RespBeanEnum.EMPTY_STOCK);
        }

//        Order order = orderService.secKill(user,goods);

        SecKillMessage secKillMessage = new SecKillMessage(user, goodsId);
        mqSender.sendSecKillMessage(JsonUtil.object2JsonStr(secKillMessage));

        //接收为0，返回“排队中”
        return RespBean.success(0);
    }


    /**
     * 获取秒杀结果
     * @param user
     * @param goodsId
     * @return orderId：成功；-1：秒杀失败；0：排队中
     */
    @RequestMapping(value = "/result",method = RequestMethod.GET)
    @ResponseBody
    public RespBean getResult(User user,Long goodsId){
        if (user == null){
            return RespBean.error(RespBeanEnum.SESSION_ERROR);
        }

        Long  orderId = seckillOrderService.getResult(user,goodsId);
        return RespBean.success(orderId);
    }


    /**
     * 获取秒杀地址
     * @param user
     * @param goodsId
     * @return
     */
    @AccessLimit(second = 5, maxCount = 5, needLogin = true)
    @RequestMapping(value = "/path", method = RequestMethod.GET)
    @ResponseBody
    public RespBean getPath(User user, Long goodsId, String captcha, HttpServletRequest request){
        if (user == null){
            return RespBean.error(RespBeanEnum.SESSION_ERROR);
        }

//        ValueOperations valueOperations = redisTemplate.opsForValue();
//
//        //限制访问次数，5秒内访问5次
//        String uri = request.getRequestURI();
//        captcha = "0";
//        Integer count = (Integer) valueOperations.get(uri + ":" + user.getId());
//        if (count == null){
//            valueOperations.set(uri + ":" + user.getId(), 1 , 5 ,TimeUnit.SECONDS);
//        }else if (count < 5){
//            valueOperations.increment(uri + ":" + user.getId());
//        }else {
//            return RespBean.error(RespBeanEnum.ACCESS_LIMIT_REACHED);
//        }

        boolean check = orderService.checkCaptcha(user,goodsId,captcha);
        if (!check){
            return RespBean.error(RespBeanEnum.ERROR_CAPTCHA);
        }

        String path = orderService.createPath(user,goodsId);
        return RespBean.success(path);
    }


    @RequestMapping(value = "/captcha", method = RequestMethod.GET)
    public void verifyCode(User user, Long goodsId, HttpServletResponse response){
        if (user == null || goodsId<0){
            throw new GlobalException(RespBeanEnum.REQUEST_ILLEGAL);
        }

        //设置请求头为输出图片的类型
        response.setContentType("image/jpg");
        response.setHeader("Program","No-cache");
        response.setHeader("Cache-Control","No-cache");
        response.setDateHeader("Expires",0);

        //生成验证码，将结果放入redis
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(130,32,3);
        redisTemplate.opsForValue().set("captcha:" + user.getId() + ":" + goodsId,captcha.text(),300, TimeUnit.SECONDS);
        try {
            captcha.out(response.getOutputStream());
        } catch (IOException e) {
            log.info("验证码生成失败",e.getMessage());
        }
    }



    /**
     * 系统初始化，把商品库存数量加载到redis
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        List<GoodsVo> list = goodsService.findGoodsVo();
        if (CollectionUtils.isEmpty(list)){
            return;
        }
        list.forEach(goodsVo -> {
            redisTemplate.opsForValue().set("secKillGoods:"+goodsVo.getId(),goodsVo.getStockCount());
            EmptyStockMap.put(goodsVo.getId(),false);
        }
        );
    }
}

/**
 * 1.减少数据库访问：系统初始化的时候，就把商品库存数量加载到Redis里，当我们收到秒杀请求时，就可以去redis中预减库存，
 * 在短时间内不会去访问数据库，减少了访问数据库的次数。2.当redis里面库存不足的时候，直接返回，例如商品数量只有10个，在
 * redis里面扣完10个之后，不管后面从第11个开始还是到哪里，它都不会对数据库进行操作了，也是在redis扣完直接返回了，这对系统的性
 * 能来说也是一个很大的提升了。3.假如库存量是足够的，那么把秒杀的请求封装成一个对象发送给rabbitmq，因为最终还是要去
 * 生成订单的，可以把此过程异步出去，将前面的大量请求快速处理掉，后面再用消息队列慢慢处理。4.异步出去后，要返回“排队中”，
 * 因为不能确定是否下单成功，所以不能返回成功或者失败。如果返回“排队中”之后，那么真正封装出去的异步消息就会正常出队，
 * 然后生成订单，减少库存(数据库)。5.因为入队时返回的是“排队中”，最终还是要告诉用户一个结果，所以要在客户端做轮询，判断是否真的秒杀成功
 *
 * */




/**
 * 1.减少数据库访问：
 * */


/**
 * 某些人能够提前获取到秒杀接口的地址，并通过脚本或者工具不停地去点击秒杀接口，这样的刷新速度是远高于普通用户的，同时也会给服务器造成很大的压力。
 * 隐藏秒杀接口的地址：秒杀开始的时候，并不会直接调用秒杀接口，而是去获取真正的秒杀接口的地址，并且根据每个用户秒杀的商品的不同，它的地址是不一样的，
 *
 * 通过接口去获取秒杀接口的地址，再根据秒杀接口的地址去进行秒杀，这样能够避免某些人提前准备好秒杀接口，通过准备好的脚本或者工具不停地去点击
 *
 *
 * */

/**
 * 添加验证码：
 * 1.防止脚本；
 * 2.拉长秒杀的时间跨度，减少单位时间内接口的访问量
 * */

