package com.yang.seckill.controller;


import com.yang.seckill.pojo.User;
import com.yang.seckill.rabbitmq.MQSender;
import com.yang.seckill.service.IUserService;
import com.yang.seckill.utils.CookieUtil;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yang
 * @since 2022-09-05
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    private MQSender mqSender;

    @RequestMapping("/info")
    @ResponseBody
    public RespBean info(User user){
        return RespBean.success(user);
    }

    /**
     * 更新用户密码
     * @param updatePassword
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/updatePassword")
    @ResponseBody
    public RespBean updatePassword(String updatePassword, HttpServletRequest request, HttpServletResponse response){

        String ticket = CookieUtil.getCookieValue(request,"userTicket");
        RespBean password = userService.updatePassword(ticket, updatePassword, request, response);

        return password;
    }


//    /**
//     * 测试发送rabbitMQ消息
//     */
//    @RequestMapping("/mq")
//    @ResponseBody
//    public void mq(){
//        mqSender.send("hello world");
//    }
//
//    /**
//     * fanout模式
//     */
//    @ResponseBody
//    @RequestMapping("/mq/fanout")
//    public void mq01(){
//        mqSender.send("Hello fanout");
//    }
//
//    /**
//     * direct模式
//     */
//    @ResponseBody
//    @RequestMapping("/mq/direct01")
//    public void mq02(){
//        mqSender.send01("Hello red");
//    }
//
//    /**
//     * direct模式
//     */
//    @ResponseBody
//    @RequestMapping("/mq/direct02")
//    public void mq03(){
//        mqSender.send02("Hello blue");
//    }
//
//    /**
//     * topic模式
//     */
//    @ResponseBody
//    @RequestMapping("/mq/topic01")
//    public void mq04(){
//        mqSender.send03("Hello red");
//    }
//
//    /**
//     * topic模式
//     */
//    @ResponseBody
//    @RequestMapping("/mq/topic02")
//    public void mq05(){
//        mqSender.send04("Hello blue");
//    }


}
