package com.yang.seckill.controller;


import com.yang.seckill.pojo.User;
import com.yang.seckill.service.IGoodsService;
import com.yang.seckill.service.IUserService;
import com.yang.seckill.vo.DetailVo;
import com.yang.seckill.vo.GoodsVo;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yang
 * @since 2022-09-05
 */
@Controller
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private IUserService userService;
    @Autowired
    private IGoodsService goodService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ThymeleafViewResolver thymeleafViewResolver;

    /**
     * 商品跳转页(缓存商品列表页)
     * @param model
     * @param user
     * @return
     */
    @RequestMapping(value = "/toList", produces = "text/html;charset=utf-8")
    @ResponseBody
    public String toList(Model model, User user, HttpServletRequest request, HttpServletResponse response){

        /**
         * 把整个商品列表页缓存起来，需要哪些操作
         */

        //Redis中获取页面，如果不为空，直接返回页面
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String html = (String)valueOperations.get("goodsList");
        if (!StringUtils.isEmpty(html)){
            return html;
        }

        //将用户对象传到前端页面
        model.addAttribute("user",user);
        model.addAttribute("goodsList",goodService.findGoodsVo());

        //Redis中获取页面，如果为空，手动渲染，存入Redis并返回页面
        WebContext webContext = new WebContext(request,response,request.getServletContext(),request.getLocale(),model.asMap());
        html = thymeleafViewResolver.getTemplateEngine().process("goodsList", webContext);
        if (!StringUtils.isEmpty(html)){
            valueOperations.set("goodsList",html,60, TimeUnit.SECONDS);
        }

        return html;
    }

    /**
     * 跳转商品详情页
     * @return
     */
//    @RequestMapping(value = "/toDetail/{goodsId}", produces = "text/html;charset=utf-8")
//    @ResponseBody
//    public String toDetail(Model model, User user, @PathVariable Long goodsId, HttpServletRequest request, HttpServletResponse response){
//
//        ValueOperations valueOperations = redisTemplate.opsForValue();
//        //Redis中获取页面，如果不为空，直接返回页面
//        String html = (String) valueOperations.get("goodsDetail:"+goodsId);
//        if (!StringUtils.isEmpty(html)){
//            return html;
//        }
//
//        model.addAttribute("user",user);
//
//        GoodsVo goodsVo = goodService.findGoodsVoByGoodsId(goodsId);
//        Date startDate = goodsVo.getStartDate();
//        Date endDate = goodsVo.getEndDate();
//        Date nowDate = new Date();
//
//        //秒杀状态
//        int secKillStatus = 0;
//        //秒杀倒计时
//        int remainSeconds = 0;
//
//        //秒杀未开始
//        if (nowDate.before(startDate)){
//            remainSeconds =(int) ((startDate.getTime()-nowDate.getTime())/1000);
//        }else if (nowDate.after(startDate)){ //秒杀已结束
//            secKillStatus = 2;
//            remainSeconds = -1;
//        }else {    //秒杀进行中
//            remainSeconds = 0;
//            secKillStatus = 1;
//        }
//
//        model.addAttribute("secKillStatus",secKillStatus);
//        model.addAttribute("remainSeconds",remainSeconds);
//        model.addAttribute("goods",goodsVo);
//
//        //Redis中获取页面，如果为空，手动渲染，存入Redis并返回页面
//        WebContext webContext = new WebContext(request, response, request.getServletContext(), request.getLocale(), model.asMap());
//        html = thymeleafViewResolver.getTemplateEngine().process("goodsDetail",webContext);
//        if (!StringUtils.isEmpty(html)){
//            valueOperations.set("goodsDetail:"+goodsId,html,60,TimeUnit.SECONDS);
//        }
//
//        return html;
//    }


    /**
     * 跳转商品详情页
     * @return
     */
    @RequestMapping("/toDetail/{goodsId}")
    @ResponseBody
    public RespBean toDetail(User user, @PathVariable Long goodsId){

        GoodsVo goodsVo = goodService.findGoodsVoByGoodsId(goodsId);
        Date startDate = goodsVo.getStartDate();
        Date endDate = goodsVo.getEndDate();
        Date nowDate = new Date();

        //秒杀状态
        int secKillStatus = 0;
        //秒杀倒计时
        int remainSeconds = 0;

        //秒杀未开始
        if (nowDate.before(startDate)){
            remainSeconds =(int) ((startDate.getTime()-nowDate.getTime())/1000);
        }else if (nowDate.after(endDate)){ //秒杀已结束
            secKillStatus = 2;
            remainSeconds = -1;
        }else {    //秒杀进行中
            remainSeconds = 0;
            secKillStatus = 1;
        }

        DetailVo detailVo = new DetailVo();
        detailVo.setUser(user);
        detailVo.setGoodsVo(goodsVo);
        detailVo.setSecKillStatus(secKillStatus);
        detailVo.setRemainSeconds(remainSeconds);

        return RespBean.success(detailVo);
    }
}

/**
 * 正常来说，约定大于配置，如果有配置文件，又有配置类，则配置类是优先加载的，而先前在配置类中实现了WebMvcConfigurer接口，没有做任何处理时，则默认在此处寻找静态资源。
 * 则配置类WebConfig中实现添加静态资源路径的addResourceHandlers方法
 * */


/**
 * 为什么要做缓存？因为QPS的瓶颈最大其实就是对于数据库的操作，这里完全可以把数据库的操作提取出来放进缓存中，但不是任何数据都适合放入缓存的，一般来说，做缓存的基本上都是
 * 那种需要频繁被读取、变更很少的数据，因为放入到缓存，还得考虑到缓存和数据库的一致性问题。
 *
 * 页面缓存，因为页面全部用的是thymeleaf模板，当用户发送请求时需要从服务器端把整个数据全部放到对应的浏览器去展示，其传输量是比较大的，而且还要去数据库进行查询操作等，则
 * 完全可以把其放入redis中做缓存
 *
 * 页面静态化
 * 静态化技术：所谓的静态化技术就是 将查询好的数据填充到模板中,然后将生成的html写入到指定的文件中。页面静态化就是使用静态化技术生成html页面。网页静态化技术是为了减轻数据
 * 库的访问压力，比较适合大规模且相对变化不太频繁的数据。另外网页静态化还有利于SEO。
 *
 *
 * 对象缓存：
 * 对象缓存相比页面缓存是更细粒度的缓存。在实际项目中， 不会大规模使用页面缓存，对象缓存就是当用到用户数据的时候，可以直接从缓存中取出；比如更新用户密码、根据token来获取用
 * 户缓存对象
 *
 * */
