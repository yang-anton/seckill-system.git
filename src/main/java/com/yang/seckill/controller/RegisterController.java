package com.yang.seckill.controller;

import com.yang.seckill.service.IUserService;
import com.yang.seckill.vo.RegisterVo;
import com.yang.seckill.vo.RespBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/register")
@Slf4j
public class RegisterController {

    @Autowired
    private IUserService userService;

    @RequestMapping("/toReg")
    public String toReg(){

        return "register";

    }

    @RequestMapping("/reg")
    @ResponseBody
    public RespBean register(RegisterVo registerVo) {
        System.out.println(registerVo);

        return userService.register(registerVo);
    }

}
