package com.yang.seckill.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
@Controller
@RequestMapping("/seckillGoods")
public class SeckillGoodsController {

}
