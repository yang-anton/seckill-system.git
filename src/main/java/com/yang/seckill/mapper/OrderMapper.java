package com.yang.seckill.mapper;

import com.yang.seckill.pojo.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
public interface OrderMapper extends BaseMapper<Order> {

}
