package com.yang.seckill.mapper;

import com.yang.seckill.pojo.SeckillGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yang
 * @since 2022-09-12
 */
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

}
