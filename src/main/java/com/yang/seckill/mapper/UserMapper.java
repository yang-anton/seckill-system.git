package com.yang.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.seckill.pojo.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yang
 * @since 2022-09-05
 */
public interface UserMapper extends BaseMapper<User> {

}
