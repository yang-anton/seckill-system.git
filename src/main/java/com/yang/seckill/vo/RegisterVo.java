package com.yang.seckill.vo;

import lombok.Data;

@Data
public class RegisterVo {

    private String id;
    private String password;
    private String rePassword;

}
