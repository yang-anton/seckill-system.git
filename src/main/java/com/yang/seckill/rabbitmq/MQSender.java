package com.yang.seckill.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消息发送者
 */
@Service
@Slf4j
public class MQSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送秒杀消息
     * @param message 秒杀消息
     */
    public void sendSecKillMessage(String message){
        log.info("发送消息："+message);
        rabbitTemplate.convertAndSend("secKillExchange","secKill.message",message);
    }

}
