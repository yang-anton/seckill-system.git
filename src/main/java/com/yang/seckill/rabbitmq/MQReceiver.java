package com.yang.seckill.rabbitmq;


import com.yang.seckill.pojo.SecKillMessage;
import com.yang.seckill.pojo.SeckillOrder;
import com.yang.seckill.pojo.User;
import com.yang.seckill.service.IGoodsService;
import com.yang.seckill.service.IOrderService;
import com.yang.seckill.service.ISeckillGoodsService;
import com.yang.seckill.utils.JsonUtil;
import com.yang.seckill.vo.GoodsVo;
import com.yang.seckill.vo.RespBean;
import com.yang.seckill.vo.RespBeanEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 消息消费者(接收者)
 */
@Service
@Slf4j
public class MQReceiver {

    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IOrderService orderService;

    /**
     * 下单操作
     */
    @RabbitListener(queues = "secKillQueue")
    public void receive(String message){
        log.info("接收到的消息："+message);
        SecKillMessage secKillMessage = JsonUtil.jsonStr2Object(message, SecKillMessage.class);
        Long goodsId = secKillMessage.getGoodsId();
        User user = secKillMessage.getUser();
        GoodsVo goodsVo = goodsService.findGoodsVoByGoodsId(goodsId);

        if (goodsVo.getStockCount()<1){
            return;
        }
        //判断是否重复抢购
        SeckillOrder seckillOrder  =
                (SeckillOrder) redisTemplate.opsForValue().get("order:" + user.getId() + ":" + goodsId);
        if (seckillOrder != null){
            return ;
        }
        //下单操作
        orderService.secKill(user,goodsVo);

    }

}
