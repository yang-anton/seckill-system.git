package com.yang.seckill.service;

import com.yang.seckill.pojo.User;
import com.yang.seckill.utils.CookieUtil;
import com.yang.seckill.utils.MD5Util;
import com.yang.seckill.vo.GoodsVo;
import com.yang.seckill.vo.LoginVo;
import com.yang.seckill.vo.RegisterVo;
import com.yang.seckill.vo.RespBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private IUserService userService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;
    @Autowired
    private IGoodsService goodsService;

    @Test
    public void register(){
        RegisterVo user = new RegisterVo();
        user.setId("18569865542");
        user.setPassword("123456");
        user.setRePassword("123456");
        userService.register(user);
    }

    @Test
    public void doLogin(){
        LoginVo loginVo = new LoginVo();
        loginVo.setMobile("13000000030");
        loginVo.setPassword(MD5Util.inputPassToBackendPass("654321"));

        userService.doLogin(loginVo,request,response);
    }

    @Test
    public void findGoodsVoByGoodsId(){
        System.out.println(goodsService.findGoodsVoByGoodsId(1L));
    }

    @Test
    public void aa(){
        int a = 9;
        System.out.println(a);
        System.out.println(a++);
        System.out.println(a);
    }

}
