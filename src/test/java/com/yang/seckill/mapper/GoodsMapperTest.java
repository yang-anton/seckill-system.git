package com.yang.seckill.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoodsMapperTest {

    @Autowired
    private GoodsMapper goodsMapper;

    @Test
    public void findGoodsVo(){

        System.out.println(goodsMapper.findGoodsVo());

    }

    @Test
    public void findGoodsVoByGoodsId(){

        System.out.println(goodsMapper.findGoodsVoByGoodsId(1L));

    }

}
